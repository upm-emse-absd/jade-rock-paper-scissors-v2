package es.upm.emse.absd.agents;

import es.upm.emse.absd.Utils;
import es.upm.emse.absd.agents.behaviours.PlayBehaviour;
import jade.core.Agent;

public class PlayerAgent extends Agent {

    protected void setup()
    {

        if (Utils.register(this, "player")) {
            PlayBehaviour pb = new PlayBehaviour(this);
            addBehaviour(pb);
        }
        else doDelete();

    }

}
